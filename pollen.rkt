#lang racket/base

(require racket/function)
(require pollen/tag)
(require "util.rkt")
(require pollen/decode pollen/misc/tutorial txexpr)
(provide (all-defined-out))
(require pollen/template/html
         pollen/setup)
(require pollen/core)
(require racket/os)
(require pollen/unstable/pygments)
(require racket/list)
(require racket/match)


(define headline (default-tag-function 'h2))
(define items (default-tag-function 'ul))
(define item (default-tag-function 'li 'p))
(define (elink url text) `(a ((href ,url)) ,text))

(define (code lang . stuff)
  (apply highlight (cons lang stuff)))

(define (assign-note-numbers elt)
  (define notes
    (find-all-by (lambda (x)
                   (and (txexpr? x)
                        (equal? (get-tag x) 'note)))
                 elt))

  (define (assign-num-to-note note hsh)
    (define name (first (get-elements note)))
    ;; (define name (attr-ref note 'name null))
    (define num (+ 1 (hash-count hsh)))
    (hash-set hsh name num))

  (foldl assign-num-to-note
         (hash)
         notes))

(define (decode-note-num elt note-nums tx)
  (match (get-tag tx)
    ['note (define elts (get-elements tx))
           (define name (first elts))
           (define content (rest elts))
           (define num (hash-ref note-nums name null))

           `(span (sup ([class "note-num"]),(number->string num))
                  (span ([class "note-body"]
                        [id ,name])
                       ,(format "~a. " num)
                       ,@content))]
    [else tx]))

(define (decode-toc elt tx)
  (match (get-tag tx)
    ['toc (define term-nodes (extract-terms elt))

          (define terms
            (sort
             (flatten (map get-elements term-nodes))
             string<?))

          (define (word-to-li word)
            `(li (a ([href ,(string-append "#" (symbol->string (title->slug word)))])
                    ,word)))

          `(ul ([class "toc"])
               ,@(map word-to-li terms))]
    [else tx]))

(define (root . elements)
  (define elt (cons 'root elements))
  (define note-nums (assign-note-numbers elt))

  (define (decode-double-breaks-into-paras elements)
    (decode-paragraphs elements
                       #:linebreak-proc (λ (x) (decode-linebreaks x '" "))))

  ;; (define (decode-paras elements)
  ;;   (decode-paragraphs elements #:linebreak-proc identity))

  (define decoded
    (decode-elements elements
                     #:txexpr-proc (compose1 (curry decode-toc elt)
                                             (curry decode-note-num elt note-nums))
                     ;; #:txexpr-elements-proc decode-double-breaks-into-paras
                     #:string-proc (compose1 smart-quotes
                                             smart-dashes)))

  (txexpr 'div '([class "main"]) decoded))

(define (title . text)
  `(h1 ,@text))

(define (subtitle text)
  `(h2 ,text))

(define (glossary . elements)
  `(ul () ,@elements))

(define (introduction . elements)
  `(p () ,@elements))

(define (subhead . elements)
  `(h3 ,@elements))

(define loz "◊")

;; todo in this = omit from output
(define (todo . elements) "")

(define (term term . definition)
  ;; (displayln term)
  ;; (printf "here: ~s\n" here)
  ;; (save-ref term)

  `(li ([id ,(symbol->string (title->slug term))]
        [class "glossary-term"])
       (em ,term)

       (term ([class "hidden"]) ,term)
       (p ,@definition)))

(define (bquote . body)
  `(blockquote ,@body))

(define-tag-function (link attributes elements)
  (link- attributes elements))

(define-tag-function (linkh attributes elements)
  (->html (link- attributes elements))
  ;; (->html '(a ((href "huff")) "Bunk beds"))
  ;; (->html '(root (script "3 > 2") "Why is 3 > 2?"))
  )

(define (link- attributes elements)
  (define to (cadr (assoc 'to attributes)))
  (define dest (lookup-ref to))
  ;; (printf "dest: ~a\n" dest)
  (cond
    [(null? dest)
     (printf "warning: could not determine dest for: ~a\n" to)
     `(a ([href "#"]) ,@elements)]
    [else `(a ([href ,dest]) ,@elements)]))

(define smhr `(hr ([class "small"])))


(define (is-published? src)
  (define metas (get-metas src))
  (equal? (hash-ref metas
                    'pub-state
                    'draft)
          'published))

(define (entries-list)
  (printf "entries-list\n")
  (define entries (read-entries))
  (define entries-to-list
    (filter is-published?
            entries))
  (define (render-entry entry)
    (define slug (filename->slug entry))
    (define title (or (select 'h1 (get-doc entry)) "(Untitled)"))
    `(li ()
         ,(link- (list [list 'to
                             (list slug)])
                 (list title))))
  `(ul
    ,@(map render-entry entries-to-list)))

(define styles
  #<<STYLE
<link href="/baskerville.css" rel="stylesheet">
<link href="/style.css" rel="stylesheet">
STYLE
  )
