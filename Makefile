build:
	./build.rkt
	cp baskerville.css baskerville.ttf courgette.css courgette.ttf style.css _dist

serve:
	bash -c 'cd _dist; python3 -m http.server 8000'

default: build
