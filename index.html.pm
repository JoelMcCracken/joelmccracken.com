#lang pollen

◊title{Joel N. McCracken}

Not much to see here, for now.

◊ul{

◊li{ ◊elink["https://twitter.com/JoelMcCracken"]{twitter.com/JoelMcCracken} }

◊li{ ◊elink["https://gitlab.com/JoelMcCracken"]{gitlab.com/JoelMcCracken} }

◊li{ ◊elink["https://github.com/JoelMcCracken"]{github.com/JoelMcCracken} }

◊li{ ◊elink["https://joelmccracken.github.io/"]{joelmccracken.github.io}, my previous blog }

}

◊todo{list projects}

◊todo{list posts}


◊p{Entries:}

◊entries-list[]
