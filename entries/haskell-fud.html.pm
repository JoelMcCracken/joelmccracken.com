#lang pollen

◊(define-meta pub-state draft)
◊(define-meta pub-date "2019-03-13")
◊(define-meta type entry)
◊(define-meta aliases (haskfud))

◊title{what i used to think about haskell, and why i was wrong}



I used to be a person who *really* disliked statically typed programming languages.
I was convinced that dynamic languages were the way to go.

◊h2{How I formed that opinion}

I suppose that this may be common for many people, but my first impressions
were formed by Java.

My programming experience went JavaScript, PHP, C, C++, Java,

But once I got to Java, I began to feel the pain of its restrictions,
and really just how hard it was to do *everything*

Eventually I was exposed to python, and wow, it was so much easier to get things done!
I was very impressed. Then I learned some Lisp, Ruby, etc. And around this time I
started reading SICP, and lisp, and was so blown away that i ended up set in my ways
fo ryears



eventually i came to wish to revisit this, and learned haskell.

◊ul{

}

Static type systems do not save you from the majority of bugs

At the time I thought this, I had not been exposed to the many ways you can
encode valuable information into your type system.

one area that is discussed really often is avoiding Null's with a Maybe type, or
capturing errors with an Either type. These are both exceedingly useful, but I think
an underappreciated on is newtype wrapping

One very valuable technique is also incredibly simple: newtype wrapping.
TODO link to the newtype wrapping page and finish that first.

haskell is hard

actually, what I have found is that haskell is *easier*. Not to learn -- Haskell
is a complex language, and takes longer than average to learn. But, the process of
using and programming in Haskell is easier.

This fact has blown my mind. Once I did the work to learn Haskell and learn to use it,
I am able to write code much more quickly and easily than ever before.


what I liked about dynlags

- the REPL. Working with Lisp in a REPL was a game changer

- abilityt to operate *in vivo*. This is something that I still don't have a
good answer for, but to say that in e.g. haskell it just doesn't seem to be
a need

I have changed my mind. Now, given the choice fo a good type system vs none, I
will totally pick the good type system.

- Its not worth dealing with the type system, all of it is too complicated for
what benefit you get

- why cant i "just" not do impure things in the core logic of my app, and do all the
IO/impure things at the boundaries?

what about the edge case where i'd have to do a major refactor?




This post has given me some time to think over my experience.
It is really interesting to look back on my past self and say "I was wrong".
I wonder what I will look back with a few more years experience and say

Actually, interestingly, I find that most of the opinions I hold currently feel much less strong than they were.
Does that mean I have grown, somehow?

There are other things I have
