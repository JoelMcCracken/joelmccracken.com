#lang pollen

◊(define-meta pub-state published)
◊(define-meta pub-date "2019-06-19")
◊(define-meta type entry)
◊(define-meta aliases (why-pollen))

◊title{Pollen: Authoring with Extensible, Semantic Markup}

◊intro{

◊p{
If you have seen
◊elink["https://joelmccracken.github.io"]{my previous site},
you may have noticed that I have recently changed some things.
In fact, this is all totally new.
}

◊p{
Well, this site is written in
◊elink["https://docs.racket-lang.org/pollen/index.html"]{Pollen}.
I wanted to describe my reasoning behind this, and share my experience.
}
}

◊subhead{Wishlist}

◊p{
Several things that bothered me about my old site. Its been a couple of years,
so my memory may be somewhat
faulty◊note["i-tried"]{
Some of these may have been solvable by using a variety of methods,
especially liquid tags. It has been a long time since I worked on this,
things may have changed, etc.
I tried some ways of handling these issues,
but none of the options were very good.}:
}

◊ul{
◊li{
◊p{
I started writing the posts in Markdown.
Markdown is good for what it attempts to do.
It is great for basic formatting needs. However, if I am going to
spend hours working on a project, I generally get to the point
where I want other features. Fundamentally, Markdown lacks extensibility.
}

◊p{
For example, I have worked on a few series of posts. I wanted
there to be nice navigation between the posts, and make it clear where the
reader was in the series. This was an extremely manual process, though,
requiring me to go copy and paste the markdown that would result in the list
of links, and going back to previous posts and editing them with the new
series navigation markdown.
}

◊p{
I've also wanted to change the way footnotes work and are rendered, among other things.
With Markdown, I am left without any good way to augment its rendering behavior.
}

}

◊li{
Links to resources were
◊stress{irritating}
to work with.
You might think of this as extensibility concern, but it has its own specific issues.
I can't easily change a
Specifying a URL quickly got complex.
It was tricky to get everything to work on http github.io, https github.io,
and locally.
It was tricky remembering the full path for previous pages.
This complexity led me to avoid adding links where I really should have.
}

◊li{
Varous things were manual that felt like they shouldn't be.
Specifying a post's publication date, for example.
Or, toggling a post from "draft" to "published".
Even if there were solutions for some of this, I usually didn't like them.
}

◊li{
◊p{Eventually, I grew tired enough of Markdown that I implemented a Jekyll extension
for compiling org-mode files to html, and then interpolating that into a template.
While writing with org-mode is a good deal more pleasant,
However, while writing posts in org-mode was much more pleasant
it was also ◊stress{even less} extensible!
At least Markdown lets you embed HTML, which is rendered straight out.}

◊p{Now, I could have decided to heavily customize the org-mode rendering process.
And I strongly considered doing that.
But, experience told me that I just didn't want to do it.
}
}
}

◊p{
I knew what I wanted to do: I wanted to separate the authoring from the presentation.
I wanted to author my blog posts and mark them up: here's a definition,
there's a footnote, here is a block of code that should be evaluated in a certain way,
etc.
With those tools I had, I did not have any good way to do this.
}

◊p{
Excluding XML, which I mention next, nothing like this existed.
I wasn't ready to work on a project like this yet.
Unfortunately, these little irritations piled up to the point where
I lost interest in writing.
}

◊subhead{But, XML?}

◊p{
At this point, you may be thinking "well that's what XML does!"
Semantic markup is one of its stated goals.
XSLT is even designed around this specific
"convert from semantic markup to presentation format".
}

◊p{
While some people have a deep hatred of XML, I do not. But, I don't exactly love it,
either. I am at best indifferent, though I have been tempted to try writing
in XML as a sort of iconoclastic rebellion.
But, still, I did not want to go down the route of building a whole authoring pipeline,
only to discover that I, too, hated authoring with XML.
}

◊subhead{Enter Pollen}

◊p{
Then, relatively recently, I discovered
◊elink["https://docs.racket-lang.org/pollen/index.html"]{Pollen}.
I have been curious about Racket◊note["racket"]{
Really, I have loved Racket, even if it has mostly been from afar.
◊elink["https://felleisen.org/matthias/manifesto/"]{The Racket Manifesto}
resonates with me very deeply.
This story goes back a long time to when I was still in school, and
Racket saved me from (possibly) giving up on CS. Thankfully, because
of then-PLT-Scheme SICP, I did not, but this is a story for
another time.
} for a really long time.
}

◊p{
When I read about what pollen was designed to do, I was really excited.
This tool is exactly what I want! At this point, I encourage you to
check out the Pollen
◊elink["https://docs.racket-lang.org/pollen/quick-tour.html"]{quick tour},
to get a general idea of what it looks like.
}

◊p{
Pollen is designed for authors to write with with semantic meaning.
As soon as I saw it, I knew it was just about exactly what I wanted.
}

◊smhr

◊p{
So far, I have found Pollen to be a delight. It is a little bit quirky, but
I was able to wrangle it to my will after a few evenings of hacking.
I have implemented some complex functionality and have satisfied myself that
it is going to do what I want. I get the feeling that some day I will rewrite
a lot of this in Haskell◊note["haskell-rewrite"]{
Specifically, I am thinking that I will end up porting the infrastructure code
to Haskell. There are several things that I'd like to behave differently, and
having it all in Haskell would just make things easier.
}, but overall I am quite pleased.
}
