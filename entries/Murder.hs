module Murder where

-- https://xmonader.github.io/prolog/2018/12/21/solving-murder-prolog.html
import Control.Monad
import Data.List

rooms :: [Room]
rooms = ["Bathroom", "Dining Room", "Kitchen", "Living Room", "Pantry", "Study"]

women :: [Person]
women = (Person Woman) <$> ["Barbara", "Christine", "Yolanda"]

men :: [Person]
men = (Person Man) <$> ["George", "John", "Robert"]

people :: [Person]
people = men ++ women

weapons :: [Weapon]
weapons = ["Bag", "Firearm", "Gas", "Knife", "Poison", "Rope"]

type Room = String
type Name = String
type Weapon = String

data Gender
  = Man | Woman
  deriving (Eq, Show)

data Person
  = Person Gender Name
  deriving (Show, Eq)

data RoomSln
  = RoomSln { rsRoom :: Room, rsPerson :: Person, rsWeapon :: Weapon }
  deriving (Show, Eq)

possibilities :: [ RoomSln ]
possibilities = do
  let ensureIfThen cond' implication =
        guard $ if cond' then implication else True
  room <- rooms
  let inStudy = room == "Study"
  let inKitchen = room == "Kitchen"
  let inPantry = room == "Pantry"
  let inLivingRoom = room == "Living Room"
  let inDiningRoom = room == "Dining Room"
  let isRooms = any (room ==)

  person@(Person gender name) <- people
  let isMan = gender == Man
  let isWoman = gender == Woman

  let isYolanda = name == "Yolanda"
  let isGeorge = name == "George"

  let isPersons = any (name ==)

  -- 1
  ensureIfThen inKitchen isMan

  -- 2
  ensureIfThen
    (isPersons ["Barbara", "Yolanda"])
    (isRooms ["Study", "Bathroom"])

  -- 4
  ensureIfThen inStudy isWoman

  -- 5
  ensureIfThen inLivingRoom  $ isPersons ["John", "George"]

  -- 7
  ensureIfThen isYolanda (not $ isRooms ["Study", "Pantry"])

  weapon <- weapons
  let isRope = weapon == "Rope"
  let isBag = weapon == "Bag"
  let isKnife = weapon == "Knife"
  let isFirearm = weapon == "Firearm"
  let isGas = weapon == "Gas"

  let isWeapons = any (weapon ==)


  -- 1
  ensureIfThen inKitchen (not $ isWeapons ["Rope", "Knife", "Bag", "Firearm"])

  -- 3
  ensureIfThen isBag (not $ isPersons ["Barba", "George"])
  ensureIfThen isBag (not $ isRooms ["Bathroom", "Dining Room"])

  -- 4
  ensureIfThen inStudy isRope

  -- 6
  ensureIfThen inDiningRoom  (not isKnife)

  -- 8
  ensureIfThen isFirearm isGeorge

  -- etc
  ensureIfThen inPantry isGas

  [RoomSln room person weapon]

hasDuplicate :: Eq a => [a] -> Bool
hasDuplicate xs =
  length xs /= (length $ nub xs)

hasRepetition :: [RoomSln] -> Bool
hasRepetition slns =
  let
    x = hasDuplicate $ rsRoom <$> slns
    y = hasDuplicate $ rsPerson <$> slns
    z = hasDuplicate $ rsWeapon <$> slns
  in
    (x || y || z)

rsCombinations :: [RoomSln] -> [[RoomSln]]
rsCombinations slns = do
  n1 <- slns
  n2 <- slns
  guard $ not $ hasRepetition [n1, n2]
  n3 <- slns
  guard $ not $ hasRepetition [n1, n2, n3]
  n4 <- slns
  guard $ not $ hasRepetition [n1, n2, n3, n4]
  n5 <- slns
  guard $ not $ hasRepetition [n1, n2, n3, n4, n5]
  n6 <- slns
  guard $ not $ hasRepetition [n1, n2, n3, n4, n5, n6]
  [[n1, n2, n3, n4, n5, n6]]

main = do
  putStrLn $ show $ take 1 $ rsCombinations possibilities
  putStrLn "done"
