#lang pollen

◊(define-meta pub-state draft)
◊(define-meta pub-date "2019-03-13")
◊(define-meta type entry)
◊(define-meta aliases (jargon))

◊title{Glossary of Functional Programming Jargon}

◊subtitle{(A work in progress)}

◊introduction{

At first, all the new terms in the functional programming world can feel overwhelming
to any neophyte. So, I thought it would be helpful to lay out some of these terms in
a way that could be handy for future reference, in a way to give a person enough
context. I wish such a thing existed when I was starting out!

One of the things that is most missing in references is how things are used. For
example, I could give the definition of an isomorphism. However, does that adequately
explain what it means when someone says that two things are isomorphic to each other?
I don't think so.

Another
◊elink["https://github.com/hemanth/functional-programming-jargon"]{
"jargon glossary"}
exists, and I think its a wonderful resource, but I want to take
a different approach.

I am working on this with the goal to make it helpful to a newcomer. My goal is to
favor intuition over rigor, and friendliness over exactness. Addtionally, these
definitions are all ◊em{as I understand them}. Its possible that there is something
not quite right somewhere.

Have anything to suggest? Please, ◊elink["https://gitlab.com/JoelMcCracken/joelmccracken.com/issues"]{submit an issue}.
}

◊h3{Table of Contents}

◊toc[]

◊h3{Terms}

◊glossary{

◊term["Category Theory"]{

My favorite definition of cateogry theory:

◊bquote{

Category Theory is a branch of Mathematics mainly used by haskellers to annoy everyone else. But, believe it or not, it's also useful to reason about programs.

-- from
◊elink["https://www.javiercasas.com/articles/codata-in-action"]{Codata In Action},
by Javier Casas
}

The FP community often steals ideas from category theory and brings
them into programming languages. It gives us some ideas about how to design
some abstractions, so that they work together better.

You do not need to understand category theory to succeed at functional programming.
But, if it interests you, it ◊em{is} an excellent topic to explore!

For more information, read the oft-recommended series of articles
◊elink["https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface/"]{Category Theory for Programmers}.
}
◊term["Functor"]{
The shortest way to describe a functor is that it is something that is "mappable".
Odds are you have seen mapping over a list. For example, in JavaScript:

◊code['JavaScript]{
const x = [1,2,3];
x.map((i) => i + 1) // results in `Array(3) [ 2, 3, 4 ]`
}
}

◊term["Monad"]
◊term["Monoid"]
◊term["Semigroup"]

◊todo{

◊term["Applicative"]
◊term["Traversable"]
◊term["Lattice"]
◊term["map"]
◊term["function"]
◊term["immutable, immutability"]
◊term["pure"]
◊term["pure fp"]
◊term["referential transparency"]
◊term["Haskell"]
◊term["Purescript"]
◊term["Elm"]
◊term["JavaScript"]
◊term["group"]
}
}
