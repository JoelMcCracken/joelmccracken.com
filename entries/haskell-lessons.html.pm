#lang pollen

#lang pollen

◊(define-meta pub-state draft)
◊(define-meta pub-date "2019-03-13")
◊(define-meta type entry)
◊(define-meta aliases (hasklesson))

write a

lambda calculus interactive book

illustrate new concepts by showing why we want them
(make it painful, then show the solution)

GHCI + reference

Stack info

imports, exports, etc

motivate w/ lessons
e.g. print names over and over, typing each one out
then introduce functions

build standard libs for each kind of thing
monoid, semigroup, functor, applicative, monad, traversable, etc

chapter on testing,

basics (infra, etc)

lambda calculus

misc./appendicies

importing things

haskell modules

the orphan instance problem

suggested resources
