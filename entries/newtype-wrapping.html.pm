#lang pollen

◊(define-meta pub-state draft)
◊(define-meta pub-date "2019-03-13")
◊(define-meta type entry)
◊(define-meta aliases (newtype-wrapping))

◊title{Newtype Wrapping}

One very valuable technique is also incredibly simple: newtype wrapping.

A newtype is a way to add a "new type" to a value, wrapping it within the context of a new type. Some examples:

◊code["haskell"]{

newtype Name = Name String

sayHelloTo

}

Just this single feature is so nice -- it makes your code easier to work with,
